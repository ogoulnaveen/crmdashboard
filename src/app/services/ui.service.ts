
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable()
export class UiService {

    constructor() { }

    public langDataSource = new BehaviorSubject('en');
    langValue = this.langDataSource.asObservable();
    
    private dashboardTypeSource = new BehaviorSubject<any>({});
    dashboardType = this.dashboardTypeSource.asObservable();

    setdashboardType(val) {
        this.dashboardTypeSource.next(val);
    }
}
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from './services/ui.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'azia';

  constructor(
    private translate: TranslateService,
    private _uiService: UiService
  ) {
    this.translate.addLangs(['en', 'ar']);
    this.translate.setDefaultLang('en');
    this._uiService.langValue.subscribe(val => {
      this.translate.use(val);
    })
  }
  ngOnInit() {
    
  }
}

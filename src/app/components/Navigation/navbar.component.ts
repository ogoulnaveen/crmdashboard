import { Component, OnInit } from '@angular/core';
import { UiService } from 'src/app/services/ui.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-navbar-component',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
    currentDashboard = 'sales';
    lang = {
        code: 'en',
        name: 'English'
    }
    constructor(
        private _uiService: UiService,
        private translate: TranslateService,
        private router: Router
    ) {
    }

    ngOnInit() {

    }
    onChangeDashboradType(ev){
        if(ev.target.value == "sales"){
            this.router.navigateByUrl('sales/dashboard');
        }else if(ev.target.value == "marketing"){
            this.router.navigateByUrl('marketing/dashboard');
        }else if(ev.target.value == "service"){
            this.router.navigateByUrl('service/dashboard');
        }
        
        this.currentDashboard = ev.target.value;
        this._uiService.setdashboardType(ev.target.value);
    }
    onLanguageSelect(ev) {
        this.translate.use(ev.target.value);
        alert(ev.target.value);
        this._uiService.langDataSource.next(ev.target.value);
    }
}
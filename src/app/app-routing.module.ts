import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UiService } from './services/ui.service';

import { SalesDashboardComponent } from './components/Sales/dashboard/sales-dashboard.component';

import { SalescustomersComponent } from './components/Sales/customers/SalesCustomers.component';

import { MarketingdashboardComponent } from './components/Marketing/dashboard/MarketingDashboard.component';

import { MarketingcontactsComponent } from './components/Marketing/contacts/MarketingContacts.component';

import { ServicedashboardComponent } from './components/Service/dashboard/ServiceDashboard.component';

import { ServicecasesComponent } from './components/Service/cases/ServiceCases.component';
import { MarketingLeadsComponent } from './components/Marketing/leads/marketing-leads.component';
import { MarketingcustomersComponent } from './components/Marketing/customers/marketingCustomers.component';
const routes: Routes = [
  { path: '', redirectTo: '/sales/dashboard', pathMatch: 'full' },
  {path: 'sales/dashboard' , component: SalesDashboardComponent},
  {path: 'sales/customers' , component: SalescustomersComponent},
  {path: 'service/dashboard' , component: ServicedashboardComponent},
  {path: 'service/cases' , component: ServicecasesComponent},

  {path: 'marketing/dashboard' , component: MarketingdashboardComponent},
  {path: 'marketing/contacts' , component: MarketingcontactsComponent},
  {path: 'marketing/leads' , component: MarketingLeadsComponent},
  {path: 'marketing/customers' , component: MarketingcustomersComponent},
];

//import { NavbarComponent } from './components/Navigation/navbar.component';



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
